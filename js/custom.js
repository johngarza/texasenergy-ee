$(document).ready(function() {

	///////////////////////////////////////////////////////////////////////////////////
	//ANCHOR - class="scroll" or class="top"
	///////////////////////////////////////////////////////////////////////////////////
	$("a.scroll").each(function(){
	$(this).click(function() {
		var targetName = $(this).attr("href");	
		//find out offset and subtrace the size of our header (150)
		var offset = $(targetName).offset().top - 150;
		
		$('html, body').animate({scrollTop: offset}, 800);
		return false;
		});
	});
	
	$("p.top").click(function(){
		$('html, body').animate({scrollTop: 0}, 800);
	});
	
	///////////////////////////////////////////////////////////////////////////////////
	//TEMPLATE PAGE NAVIGATION
	///////////////////////////////////////////////////////////////////////////////////
	$(".lvl2-toggleWrap").hide();
	
	//Slide up and down on click
	$(".lvl2-trigger").click(function(){
		$(this).next(".lvl2-toggleWrap").slideToggle("slow");
	});
		
});

// This script requires jQuery (tested with v1.7.1)

// When the page loads, hide the drawer, and attach events to the drawer button
jQuery(document).ready(function() {
	// First thing we do is hide the drawer-content, with no effects or animation
	jQuery('#lvl2-drawer-content').hide();
	
	// Next we set up the button's click handler
	jQuery('#lvl2-drawer-button').click(function() {
		// This line slides the drawer-content in and out
		jQuery('#lvl2-drawer-content').animate({height: 'toggle'}, 500);
		jQuery('#lvl2-drawer-button').toggleClass('lvl2-close');
	});
});
