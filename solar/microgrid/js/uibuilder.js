jQuery(document).ready(function($) {

	//-- Cached Elements
	var carousel = $('#carousel');
	var modal = $('#modal');
	var result = $('#modal .result');
	
	//-- Carousel Config
	carousel.jcarousel({
        // Configuration goes here
    });
	
	//-- Modal Config
	modal.dialog({
		autoOpen: false,
		resizable: true,
		modal: true, 
		draggable: false,
		closeOnEscape: true,
		width: 960,
		height: 570
	});
	
			
	//-- Closes the modal when grey layer is clicked			
	$(".ui-widget-overlay").live("click", function() {
		document.getElementById('silverlightControlHost').style.visibility = "visible";
		var productElement = document.getElementById("youtubevideo");
		if (productElement != null)
		{
			document.getElementById('youtubevideo').src = "";
		}
		modal.dialog("close");
	});
	
	//-- Closes the modal when title bar is clicked			
	$(".ui-dialog-titlebar").live("click", function() {
		document.getElementById('silverlightControlHost').style.visibility = "visible";
		var productElement = document.getElementById("youtubevideo");
		if (productElement != null)
		{
			document.getElementById('youtubevideo').src = "";
		}
		modal.dialog("close");
	});
	
	var xhrMap = {};
	
	//-- Open Modal when carousel modal is clicked
	carousel.delegate("a", "click", function(){
		var currentTitle = $(this).attr("title");
		document.getElementById('silverlightControlHost').style.visibility = "hidden"; 
		
		$.ajax({
			url: $(this).attr("href"),
			success: function(data, status, xhr) {
				result.html(data);
				//xhrMap[ href ] = data;				
			},
			
			complete: function (XMLHttpRequest, textStatus) {
				var headers = XMLHttpRequest.getAllResponseHeaders();
			}			

		});
		
		modal.dialog("open").dialog("option", "title", currentTitle);
					
		return false;
	});

});